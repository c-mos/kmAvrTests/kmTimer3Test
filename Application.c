/*
 * Application.c
 *
 *  Created on: Jan 16, 2024
 *      Author: Krzysztof Moskwa
 *      License: GPL-3.0-or-later
 *
 *  kmTimer3Test Application for testing kmTimer3 library from AVR kmFramework
 *  Copyright (C) 2024  Krzysztof Moskwa
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <stdlib.h>
#include <stdio.h>
#include <limits.h>

#include <avr/interrupt.h>
#include <avr/pgmspace.h>
#include <util/delay.h>

#include "kmCpu/kmCpu.h"
#include "kmDebug/kmDebug.h"


#include "kmTimersCommon/kmTimerDefs.h"
#include "kmTimer3/kmTimer3.h"

#define KM_TIMER3_TEST_1MS 1000 // number of microseconds defining 1 millisecond
#define KM_TIMER3_TEST_5MS 5000 // number of microseconds defining 5 millisecond
#define KM_TIMER3_TEST_USER_DATA_A 1UL
#define KM_TIMER3_TEST_USER_DATA_B 65535UL
#define KM_TIMER3_TEST_USER_DATA_C 255UL
#define KM_TIMER3_TEST_USER_DATA_D 4096UL
#define KM_TIMER3_TEST_DUTY_0_PERC KM_TIMER3_BOTTOM
#define KM_TIMER3_TEST_DUTY_25_PERC KM_TIMER3_MID - (KM_TIMER3_MID >> KMC_DIV_BY_2)
#define KM_TIMER3_TEST_DUTY_50_PERC KM_TIMER3_MID
#define KM_TIMER3_TEST_DUTY_75_PERC KM_TIMER3_MID + (KM_TIMER3_MID >> KMC_DIV_BY_2)
#define KM_TIMER3_TEST_DUTY_100_PERC KM_TIMER3_MAX

#define KM_TIMER3_TEST_PHASE_45_DEG KM_TIMER3_TEST_DUTY_25_PERC
#define KM_TIMER3_TEST_PHASE_90_DEG KM_TIMER3_TEST_DUTY_50_PERC
#define KM_TIMER3_TEST_PHASE_135_DEG KM_TIMER3_TEST_DUTY_75_PERC

// "private" variables
static const uint16_t swipeTestValues[] = { 0x0000, 0x0100, 0x1000, 0x2000, 0x4000, 0x8000, 0xc000, 0xFA00, 0xFF00, 0xFE00, 0x80 };

// "private" functions - declarations
uint16_t getSwipeTestRange(void);
void callbackOVF(void *userData);
void callbackOVFToggle(void *userData);
void callbackCompAToggle(void *userData);
void callbackCompBToggle(void *userData);
void callbackCompCToggle(void *userData);
void callbackCompAOff(void *userData);
void callbackCompBOff(void *userData);
void callbackCompCOff(void *userData);
void testSwipeCompATable(void);
void testSwipeCompA(void);
void testSwipeCompBTable(void);
void testSwipeCompB(void);
void testSwipeCompCTable(void);
void testSwipeCompC(void);
void testSwipePwm(const Tcc3PwmOut pwmOut, uint16_t maxValue);
void testSwipePwmTable(const Tcc3PwmOut pwmOut);
void appTest0(void);
void appTest1(void);
void appTest2(void);
void appTest3(void);
void appTest4(void);
void appTest5(void);
void appTest6(void);
void appTest7(void);
void appTest8(void);
void appInitDebug(void);

// "private" functions - definitions
uint16_t getSwipeTestRange(void) {
	return sizeof(swipeTestValues) / sizeof(swipeTestValues[0]);
}

void callbackOVF(void *userData) {
	//	uint16_t a = (int16_t)userData;
	dbToggle(DB_PIN_0);
	dbOn(DB_PIN_1);
	dbOn(DB_PIN_2);
	dbOn(DB_PIN_3);
}

void callbackOVFToggle(void *userData) {
	//	uint16_t a = (int16_t)userData;
	dbToggle(DB_PIN_0);
}

void callbackCompAToggle(void *userData) {
	//	int16_t a = (int16_t)userData;
	dbToggle(DB_PIN_1);
}

void callbackCompBToggle(void *userData) {
//	int16_t a = (int16_t)userData;
	dbToggle(DB_PIN_2);
}

void callbackCompCToggle(void *userData) {
//	int16_t a = (int16_t)userData;
	dbToggle(DB_PIN_3);
}

void callbackCompAOff(void *userData) {
//	int16_t a = (int16_t)userData;
	dbOff(DB_PIN_1);
}

void callbackCompBOff(void *userData) {
//	int16_t a = (int16_t)userData;
	dbOff(DB_PIN_2);
}

void callbackCompCOff(void *userData) {
//	int16_t a = (int16_t)userData;
	dbOff(DB_PIN_3);
}

void testSwipeCompATable(void) {
	uint16_t swipeRange = getSwipeTestRange();
	for (int i = 0; i < swipeRange; i++) {
		_delay_ms(KM_TIMER3_TEST_SWIPE_TABLE_DELAY);
		dbToggle(DB_PIN_4);
		kmTimer3SetValueCompA(swipeTestValues[i]);
	}
}

void testSwipeCompA(void) {
	for (uint16_t i = 0; i < KM_TIMER3_MAX - KM_TIMER3_TEST_SWIPE_ACCURACY; i += KM_TIMER3_TEST_SWIPE_ACCURACY) {
		_delay_ms(KM_TIMER3_TEST_SWIPE_DELAY);
		dbToggle(DB_PIN_4);
		kmTimer3SetValueCompA(i);
	}
}

void testSwipeCompBTable(void) {
	uint16_t swipeRange = getSwipeTestRange();
	for (uint16_t i = 0; i < swipeRange; i++) {
		_delay_ms(KM_TIMER3_TEST_SWIPE_TABLE_DELAY);
		dbToggle(DB_PIN_4);
		kmTimer3SetValueCompB(swipeTestValues[i]);
	}
}

void testSwipeCompB(void) {
	for (uint16_t i = 0; i < KM_TIMER3_MAX - KM_TIMER3_TEST_SWIPE_ACCURACY; i += KM_TIMER3_TEST_SWIPE_ACCURACY) {
		_delay_ms(KM_TIMER3_TEST_SWIPE_DELAY);
		dbToggle(DB_PIN_4);
		kmTimer3SetValueCompB(i);
	}
}

#ifdef COM3C0
void testSwipeCompCTable(void) {
	uint16_t swipeRange = getSwipeTestRange();
	for (uint16_t i = 0; i < swipeRange; i++) {
		_delay_ms(KM_TIMER3_TEST_SWIPE_TABLE_DELAY);
		dbToggle(DB_PIN_4);
		kmTimer3SetValueCompC(swipeTestValues[i]);
	}
}

void testSwipeCompC(void) {
	for (uint16_t i = 0; i < KM_TIMER3_MAX - KM_TIMER3_TEST_SWIPE_ACCURACY; i += KM_TIMER3_TEST_SWIPE_ACCURACY) {
		_delay_ms(KM_TIMER3_TEST_SWIPE_DELAY);
		dbToggle(DB_PIN_4);
		kmTimer3SetValueCompC(i);
	}
}
#endif /* COM3C0 */

void testSwipePwm(const Tcc3PwmOut pwmOut, uint16_t maxValue) {
	//kmTimer3SetPwmInversion(pwmOut, false);
	for (uint16_t i = 0; i <= maxValue - KM_TIMER3_TEST_SWIPE_ACCURACY; i+=KM_TIMER3_TEST_SWIPE_ACCURACY) {
		kmTimer3SetPwmDutyBottomToTop(pwmOut, i);
		dbToggle(DB_PIN_4);
		_delay_ms(KM_TIMER3_TEST_SWIPE_DELAY);
	}

	//kmTimer3SetPwmInversion(pwmOut, true);
	for (uint16_t i = maxValue; i >= KM_TIMER3_TEST_SWIPE_ACCURACY; i-=KM_TIMER3_TEST_SWIPE_ACCURACY) {
		kmTimer3SetPwmDutyBottomToTop(pwmOut, i);
		dbToggle(DB_PIN_4);
		_delay_ms(KM_TIMER3_TEST_SWIPE_DELAY);
	}
}

void testSwipePwmTable(const Tcc3PwmOut pwmOut) {
	uint16_t swipeRange = getSwipeTestRange();

	kmTimer3SetPwmInversion(pwmOut, false);
	for (int i = 0; i < swipeRange; i++) {
		_delay_ms(KM_TIMER3_TEST_SWIPE_TABLE_DELAY);
		dbToggle(DB_PIN_4);
		kmTimer3SetPwmDutyBottomToTop(pwmOut, swipeTestValues[i]);
	}
	for (int i = 0; i < 10; i++) {
		_delay_ms(10);
		dbToggle(DB_PIN_4);
	}
	kmTimer3SetPwmInversion(pwmOut, true);
	for (int i = 0; i < swipeRange; i++) {
		_delay_ms(KM_TIMER3_TEST_SWIPE_TABLE_DELAY);
		dbToggle(DB_PIN_4);
		kmTimer3SetPwmDutyBottomToTop(pwmOut, swipeTestValues[i]);
	}
}

void appTest0(void) {
	kmTimer3InitOnAccuratePeriodGenerateOutputClockA(KM_TIMER3_TEST_1MS);
	kmTimer3Start();
}

void appTest1(void) {
	kmTimer3InitOnAccurateTimeCompAInterruptCallback(KM_TIMER3_TEST_5MS, true);

#ifdef KM_TIMER3_TEST_INTERRUPTS
	kmTimer3RegisterCallbackCompA(KM_TIMER3_USER_DATA(KM_TIMER3_TEST_USER_DATA_A), callbackCompAToggle);
	kmTimer3EnableInterruptCompA();
#endif /* KM_TIMER3_TEST_INTERRUPTS */

	kmTimer3Start();
}

void appTest2(void) {
	kmTimer3InitOnPrescalerBottomToTopOvfCompABCInterruptCallback(KM_TCC0_PRSC_8);

#ifdef KM_TIMER3_TEST_INTERRUPTS
	kmTimer3RegisterCallbackOVF(KM_TIMER3_USER_DATA(KM_TIMER3_TEST_USER_DATA_D), callbackOVFToggle);
	kmTimer3EnableInterruptOVF();

	kmTimer3RegisterCallbackCompA(KM_TIMER3_USER_DATA(KM_TIMER3_TEST_USER_DATA_A), callbackCompAToggle);
	kmTimer3SetValueCompA(KM_TIMER3_TEST_DUTY_25_PERC);
	kmTimer3EnableInterruptCompA();

#ifdef OCR3B
	kmTimer3RegisterCallbackCompB(KM_TIMER3_USER_DATA(KM_TIMER3_TEST_USER_DATA_B), callbackCompBToggle);
	kmTimer3SetValueCompB(KM_TIMER3_TEST_DUTY_50_PERC);
	kmTimer3EnableInterruptCompB();
#endif /* OCR3B */
#ifdef OCR3C
	kmTimer3RegisterCallbackCompC(KM_TIMER3_USER_DATA(KM_TIMER3_TEST_USER_DATA_C), callbackCompCToggle);
	kmTimer3SetValueCompC(KM_TIMER3_TEST_DUTY_75_PERC);
	kmTimer3EnableInterruptCompC();
#endif /* OCR3C */
#endif /* KM_TIMER3_TEST_INTERRUPTS */

	kmTimer3ConfigureOCA(KM_TCC3_A_COMP_OUT_TOGGLE);
#ifdef OCR3B
	kmTimer3ConfigureOCB(KM_TCC3_B_COMP_OUT_TOGGLE);
#endif /* OCR3B */
#ifdef OCR3C
	kmTimer3ConfigureOCC(KM_TCC3_C_COMP_OUT_TOGGLE);
#endif /* OCR3C */

	kmTimer3Start();

#ifdef KM_TIMER3_TEST_SWIPE
	testSwipeCompA();
	kmTimer3SetValueCompA(KM_TIMER3_MID);
#ifdef OCR3B
	testSwipeCompB();
	kmTimer3SetValueCompB(KM_TIMER3_MID);
#endif /* OCR3B */
#ifdef OCR3C
	testSwipeCompC();
	kmTimer3SetValueCompC(KM_TIMER3_MID);
#endif /* OCR3C */
#endif /* KM_TIMER3_TEST_SWIPE */

}

void appTest3(void) {
	kmTimer3InitOnAccurateTimeCompABInterruptCallback(KM_TIMER3_TEST_1MS, KM_TIMER3_TEST_PHASE_45_DEG); // for 1 millisecond

#ifdef KM_TIMER3_TEST_INTERRUPTS
	kmTimer3RegisterCallbackCompA(KM_TIMER3_USER_DATA(KM_TIMER3_TEST_USER_DATA_A), callbackCompAToggle);
	kmTimer3EnableInterruptCompA();

#ifdef OCR3B
	kmTimer3RegisterCallbackCompB(KM_TIMER3_USER_DATA(KM_TIMER3_TEST_USER_DATA_B), callbackCompBToggle);
	kmTimer3EnableInterruptCompB();
#endif /* OCR3B */

#endif /* KM_TIMER3_TEST_INTERRUPTS */
	kmTimer3Start();
}

void appTest4(void) {
#ifdef COM3C0
	kmTimer3InitOnPrescalerBottomToTopFastPwm(KM_TCC3_PRSC_1, KM_TIMER3_TEST_DUTY_25_PERC, false, KM_TIMER3_TEST_DUTY_50_PERC, false, KM_TIMER3_TEST_DUTY_75_PERC, false);
#else /* COM3C0 */
	kmTimer3InitOnPrescalerBottomToTopFastPwm(KM_TCC3_PRSC_1, KM_TIMER3_TEST_DUTY_25_PERC, false, KM_TIMER3_TEST_DUTY_50_PERC, false);
#endif /* COM3C0 */

#ifdef KM_TIMER3_TEST_INTERRUPTS
	kmTimer3RegisterCallbackOVF(KM_TIMER3_USER_DATA(KM_TIMER3_TEST_USER_DATA_D), callbackOVFToggle);
	kmTimer3EnableInterruptOVF();

	kmTimer3RegisterCallbackCompA(KM_TIMER3_USER_DATA(KM_TIMER3_TEST_USER_DATA_A), callbackCompAToggle);
	kmTimer3EnableInterruptCompA();
#ifdef OCR3B
	kmTimer3RegisterCallbackCompB(KM_TIMER3_USER_DATA(KM_TIMER3_TEST_USER_DATA_B), callbackCompBToggle);
	kmTimer3EnableInterruptCompB();
#endif /* OCR3B */

#ifdef OCR3C
	kmTimer3RegisterCallbackCompC(KM_TIMER3_USER_DATA(KM_TIMER3_TEST_USER_DATA_C), callbackCompCToggle);
	kmTimer3EnableInterruptCompC();
#endif /* OCR3C */
#endif /* KM_TIMER3_TEST_INTERRUPTS */

	kmTimer3Start();

#ifdef KM_TIMER3_TEST_SWIPE
#ifdef OCR3C
	testSwipePwm(KM_TCC3_PWM_OUT_C, KM_TIMER3_MAX);
#endif /* OCR3B */
#ifdef OCR3B
	testSwipePwm(KM_TCC3_PWM_OUT_B, KM_TIMER3_MAX);
#endif /* OCR3B */
	testSwipePwm(KM_TCC3_PWM_OUT_A, KM_TIMER3_MAX);
#endif /* KM_TIMER3_TEST_SWIPE */
}

void appTest5(void) {
#ifdef COM3C0
	uint16_t cyclesRange = kmTimer3InitOnAccurateTimeFastPwm(KM_TIMER3_TEST_1MS, KM_TIMER3_TEST_DUTY_25_PERC, false, KM_TIMER3_TEST_DUTY_50_PERC, false, KM_TIMER3_TEST_DUTY_25_PERC, false);
#else /* COM3C0 */
	uint16_t cyclesRange = kmTimer3InitOnAccurateTimeFastPwm(KM_TIMER3_TEST_1MS, KM_TIMER3_TEST_DUTY_25_PERC, false, KM_TIMER3_TEST_DUTY_50_PERC, false);
#endif /* COM3C0 */


#ifdef KM_TIMER3_TEST_INTERRUPTS
	kmTimer3RegisterCallbackOVF(KM_TIMER3_USER_DATA(KM_TIMER3_TEST_USER_DATA_D), callbackOVFToggle);
	kmTimer3EnableInterruptOVF();

	kmTimer3RegisterCallbackCompA(KM_TIMER3_USER_DATA(KM_TIMER3_TEST_USER_DATA_A), callbackCompAToggle);
	kmTimer3EnableInterruptCompA();

#ifdef OCR3B
	kmTimer3RegisterCallbackCompB(KM_TIMER3_USER_DATA(KM_TIMER3_TEST_USER_DATA_B), callbackCompBToggle);
	kmTimer3EnableInterruptCompB();
#endif /* OCR3B */

#ifdef OCR3C
	kmTimer3RegisterCallbackCompC(KM_TIMER3_USER_DATA(KM_TIMER3_TEST_USER_DATA_C), callbackCompCToggle);
	kmTimer3EnableInterruptCompC();
#endif /* OCR3C */

#endif /* KM_TIMER3_TEST_INTERRUPTS */
	kmTimer3Start();

#ifdef KM_TIMER3_TEST_SWIPE
#ifdef OCR3C
	testSwipePwm(KM_TCC3_PWM_OUT_C, cyclesRange);
	kmTimer3SetValueCompC(cyclesRange >> KMC_DIV_BY_2);
#endif /* OCR3C */
#ifdef OCR3B
	testSwipePwm(KM_TCC3_PWM_OUT_B, cyclesRange);
	kmTimer3SetValueCompB(cyclesRange >> KMC_DIV_BY_2);
#endif /* OCR3B */
	testSwipePwm(KM_TCC3_PWM_OUT_A, cyclesRange);
	kmTimer3SetValueCompA(cyclesRange >> KMC_DIV_BY_2);
#endif /* KM_TIMER3_TEST_SWIPE */
}

void appTest6(void) {
#ifdef COM3C0
	kmTimer3InitOnPrescalerBottomToTopPcPwm(KM_TCC0_PRSC_1, KM_TIMER3_TEST_DUTY_25_PERC, false, KM_TIMER3_TEST_DUTY_50_PERC, false, KM_TIMER3_TEST_DUTY_75_PERC, false);
#else /* COM3C0 */
	kmTimer3InitOnPrescalerBottomToTopPcPwm(KM_TCC0_PRSC_1, KM_TIMER3_TEST_DUTY_25_PERC, false, KM_TIMER3_TEST_DUTY_50_PERC, false);
#endif /* COM3C0 */

#ifdef KM_TIMER3_TEST_INTERRUPTS
	kmTimer3RegisterCallbackOVF(KM_TIMER3_USER_DATA(KM_TIMER3_TEST_USER_DATA_D), callbackOVF);
	kmTimer3EnableInterruptOVF();

	kmTimer3RegisterCallbackCompA(KM_TIMER3_USER_DATA(KM_TIMER3_TEST_USER_DATA_A), callbackCompAOff);
	kmTimer3EnableInterruptCompA();
#ifdef OCR3B
	kmTimer3RegisterCallbackCompB(KM_TIMER3_USER_DATA(KM_TIMER3_TEST_USER_DATA_B), callbackCompBOff);
	kmTimer3EnableInterruptCompB();
#endif /* OCR3B */

#ifdef OCR3C
	kmTimer3RegisterCallbackCompC(KM_TIMER3_USER_DATA(KM_TIMER3_TEST_USER_DATA_C), callbackCompCOff);
	kmTimer3EnableInterruptCompC();
#endif /* OCR3C */
#endif /* KM_TIMER3_TEST_INTERRUPTS */
	kmTimer3Start();

#ifdef KM_TIMER3_TEST_SWIPE
	testSwipePwm(KM_TCC3_PWM_OUT_A, KM_TIMER3_MAX);
	kmTimer3SetValueCompA(KM_TIMER3_MID);
#ifdef OCR3B
	testSwipePwm(KM_TCC3_PWM_OUT_B, KM_TIMER3_MAX);
	kmTimer3SetValueCompB(KM_TIMER3_MID);
#endif /* OCR3B */
#ifdef OCR3C
	testSwipePwm(KM_TCC3_PWM_OUT_C, KM_TIMER3_MAX);
	kmTimer3SetValueCompC(KM_TIMER3_MID);
#endif /* OCR3C */
#endif /* KM_TIMER3_TEST_SWIPE */
}

void appTest7(void) {
#ifdef COM3C0
	uint16_t cyclesRange = kmTimer3InitOnAccurateTimePcPwm(KM_TIMER3_TEST_1MS, KM_TIMER3_TEST_DUTY_25_PERC, false, KM_TIMER3_TEST_DUTY_50_PERC, false, KM_TIMER3_TEST_DUTY_75_PERC, false);
#else /* COM3C0 */
	uint16_t cyclesRange = kmTimer3InitOnAccurateTimePcPwm(KM_TIMER3_TEST_1MS, KM_TIMER3_TEST_DUTY_25_PERC, false, KM_TIMER3_TEST_DUTY_50_PERC, false);
#endif /* COM3C0 */

#ifdef KM_TIMER3_TEST_INTERRUPTS
	kmTimer3RegisterCallbackOVF(KM_TIMER3_USER_DATA(KM_TIMER3_TEST_USER_DATA_D), callbackOVF);
	kmTimer3EnableInterruptOVF();

	kmTimer3RegisterCallbackCompA(KM_TIMER3_USER_DATA(KM_TIMER3_TEST_USER_DATA_A), callbackCompAOff);
	kmTimer3EnableInterruptCompA();
#ifdef OCR3B
	kmTimer3RegisterCallbackCompB(KM_TIMER3_USER_DATA(KM_TIMER3_TEST_USER_DATA_B), callbackCompBOff);
	kmTimer3EnableInterruptCompB();
#endif /* OCR3B */

#ifdef OCR3C
	kmTimer3RegisterCallbackCompC(KM_TIMER3_USER_DATA(KM_TIMER3_TEST_USER_DATA_C), callbackCompCOff);
	kmTimer3EnableInterruptCompC();
#endif /* OCR3C */
#endif /* KM_TIMER3_TEST_INTERRUPTS */

	kmTimer3Start();

#ifdef KM_TIMER3_TEST_SWIPE
	testSwipePwm(KM_TCC3_PWM_OUT_A, cyclesRange);
	kmTimer3SetValueCompA(cyclesRange >> KMC_DIV_BY_2);
#ifdef OCR3B
	testSwipePwm(KM_TCC3_PWM_OUT_B, cyclesRange);
	kmTimer3SetValueCompB(cyclesRange >> KMC_DIV_BY_2);
#endif /* OCR3B */
#ifdef OCR3C
	testSwipePwm(KM_TCC3_PWM_OUT_C, cyclesRange);
	kmTimer3SetValueCompC(cyclesRange >> KMC_DIV_BY_2);
#endif /* OCR3C */
#endif /* KM_TIMER3_TEST_SWIPE */
}

void appTest8(void) {
	kmTimer3Init(KM_TCC3_PRSC_1024, KM_TCC3_MODE_5_A, KM_TCC3_MODE_5_B);

	kmTimer3SetValueCompA(4);
	kmTimer3ConfigureOCA(KM_TCC3_A_PWM_COMP_OUT_CLEAR_UP_SET_DOWN);

#ifdef OCR3B
	kmTimer3SetValueCompB(9);
	kmTimer3ConfigureOCB(KM_TCC3_B_PWM_COMP_OUT_CLEAR_UP_SET_DOWN);
#endif /* OCR3B */
#ifdef OCR3C
	kmTimer3SetValueCompC(128);
	kmTimer3ConfigureOCC(KM_TCC3_C_PWM_COMP_OUT_CLEAR_UP_SET_DOWN);
#endif /* OCR3C */

	kmTimer3Start();
}

void appInitDebug(void) {
	dbPullUpAllPorts();
	dbInit();
	dbOff(DB_PIN_0);
	dbOff(DB_PIN_1);
	dbOff(DB_PIN_2);
	dbOff(DB_PIN_3);
	dbOff(DB_PIN_4);
}

// "public" functions
void appInit(void) {
	kmCpuDisableWatchdogAndInterruptsOnStartup();
	appInitDebug();
	kmCpuInterruptsEnable();

	static const uint16_t testNumber = KM_TIMER3_TEST_NUMBER;

	switch (testNumber) {
		case 0: {
			appTest0();
			break;
		}
		case 1: {
			appTest1();
			break;
		}
		case 2: {
			appTest2();
			break;
		}
		case 3: {
			appTest3();
			break;
		}
		case 4: {
			appTest4();
			break;
		}
		case 5: {
			appTest5();
			break;
		}
		case 6: {
			appTest6();
			break;
		}
		case 7: {
			appTest7();
			break;
		}
		case 8: {
			appTest8();
			break;
		}
	}
}

void appLoop(void) {
}
